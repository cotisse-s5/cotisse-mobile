import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { baseUrl } from 'src/environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  email: string;
  pwd: string;

  constructor(private httpClient: HttpClient, private storage : Storage, private router: Router) { }

  ngOnInit() {
  }

  login(){
    let user = {
      email: this.email,
      pwd: this.pwd
    };
    this.httpClient.post<any>(`${baseUrl}/user/customer/login`, user).subscribe((data)=>{
      if(data.status == 401){
        console.log(data);
			}else{
        this.storage.set('token', data.data.token).then((response) => {
          this.router.navigate(['home']);
        });
			}
    });
  }

  /*toSignUp(){
    this.route.navigate(['/login']);
  }*/
}
