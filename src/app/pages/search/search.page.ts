import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { baseUrl } from 'src/environments/environment';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
  public categs:any;
  public hours:any;
  cities:any;
  tripSearch = {
    idDeparture: String,
    idArrival: String,
    departureDate: String
  }
  listTrip:any;
  constructor(public navCtrl:NavController, private httpClient: HttpClient, private storage : Storage, private router: Router) {
  }

  ngOnInit() {
    this.getCities();
  }

  getCities(){
    this.storage.get('token').then((token) => {
      this.httpClient.get<any>(`${baseUrl}/admin/city/list`, {
        headers : {'Authorization': 'Bearer ' + token}
      }).subscribe((data)=>{
        if(data.status == 401){
          console.log(data);
        }else{
          this.cities = data.data;
        }
      });
    });
  }

  search(){
    console.log(this.tripSearch);
    this.storage.get('token').then((token) => {
       this.httpClient.post<any>(`${baseUrl}/destnDets/listDts`, this.tripSearch, {
         headers : {'Authorization': 'Bearer ' + token}
       }).subscribe((data)=>{
         if(data.status == 401){
           console.log(data);
         }else{
           console.log(data);
           this.listTrip = data.data;
         }
       });
     });
  }

  gotback(){
      this.navCtrl.back();
  }
}
