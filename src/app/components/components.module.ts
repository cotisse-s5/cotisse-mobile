import { NgModule } from '@angular/core';
import { HeaderCustComponent } from './header-cust/header-cust.component';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

@NgModule({
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        RouterModule
    ],
    exports: [HeaderCustComponent],
    declarations: [HeaderCustComponent],
    providers: [],
})

export class ComponentsModule{
    constructor(){}
}
